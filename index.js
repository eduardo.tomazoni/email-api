const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

const mongoose = require('mongoose')
const {ObjectID} = require("bson");
mongoose.Promise = global.Promise
const mongoDB = 'mongodb://localhost:27017/email-api';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true})
const conn = mongoose.connection

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/gmail.readonly'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

// Load client secrets from a local file.
fs.readFile('./credentials.json', (err, content) => {
    if (err) return console.log('Error loading client secret file:', err);
    // Authorize a client with credentials, then call the Gmail API.
    // authorize(JSON.parse(content), listLabels);

    setInterval( function() {
        authorize(JSON.parse(content), listEmails)
    }, 10000)


});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    const {client_secret, client_id, redirect_uris} = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
        client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) return getNewToken(oAuth2Client, callback);
        oAuth2Client.setCredentials(JSON.parse(token));
        callback(oAuth2Client);
    });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
    });
    console.log('Authorize this app by visiting this url:', authUrl);
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error('Error retrieving access token', err);
            oAuth2Client.setCredentials(token);
            // Store the token to disk for later program executions
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log('Token stored to', TOKEN_PATH);
            });
            callback(oAuth2Client);
        });
    });
}

/**
 * Lists the labels in the user's account.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */

function listEmails (auth) {
    const gmail = google.gmail({version:'v1', auth});
    gmail.users.messages.list({
        userId: 'me',
        labelIds: 'INBOX',
        maxResults: 1,
    }, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err);
        //console.log(res.data.messages)
        const emails = res.data.messages
        if (emails.length) {
            //console.log("emails ids:")
            emails.forEach((email) => {
                //console.log(email.id)
                gmail.users.messages.get({
                    userId: 'me',
                    id: email.id,
                }, (err, res) => {
                    if (err) return console.log('The API returned an error: ' + err);

                    const emailText = res.data.snippet  // snippet = texto da msg
                    const emailAttId = res.data.payload.parts[1].body.attachmentId // id do anexo
                    console.log("email text:")
                    console.log(emailText)
                    // console.log(emailAttId)
                    gmail.users.messages.attachments.get({
                        userId: 'me',
                        messageId: email.id,
                        id: emailAttId,
                    },(err, res) => {
                        if (err) return console.log('No attachment detected');
                        // console.log("att data:")
                        // console.log(res.data.data)
                        const b64string = res.data.data
                        const attData = {
                            _id: new ObjectID(),
                            emailData: Buffer.from(b64string, 'base64').toString()
                            }
                        // console.log("att data decoded:")
                        // console.log(attData.emailData)
                        conn.collection('emails').findOne({emailData: attData.emailData}, function (err, obj){
                            if (!obj){
                                conn.collection('emails').insertOne(attData)
                                console.log("document inserted successfully!")

                            }else{
                                console.log("document already inserted!")
                                // console.log(obj)
                            }
                        });

                    });

                });
            })
        }
    });

}